import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, TouchableOpacity } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem } from 'native-base';

export default class Kuliner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resto: [],
      isReady: false,
      property: this.props
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/foods";
    fetchData(baseUrl).then(data => {
      this.setState({ resto: data, isReady: true });
      //console.log(this.state.resto)
    });
  }
  render() {
    const resto = this.state.resto.data;
    //console.log(resto);
    if (typeof resto == 'undefined' || resto.length == 0) {
      return (<View>
        <Text style={{ alignItems: "center", alignSelf: "center", marginTop: "5%" }}>Data Tidak Ada</Text>
      </View>);
    } else {
      if (!this.state.isReady) {
        return <Expo.AppLoading />;
      } else {
        return (
          <List>
            {
              resto.slice(0, 5).map((item, idx) => {
                if (item.opening_hours) {
                  if (item.opening_hours.open_now == true) {
                    opening = "Buka";
                  } else {
                    opening = "Tutup";
                  }
                } else {
                  opening = "-";
                }
                return (
                  // <TouchableOpacity key={idx} onPress={() => this.props.navigation.navigate("Peta", { location: item })}>
                  <TouchableOpacity key={idx} onPress={() => this.state.property.navigation.navigate("Peta", { location: item })}>
                    <View style={{ flex: 1, flexDirection: 'column', borderBottomWidth: 1, borderColor: "#979797", marginTop: "1%" }}>
                      <Text style={styles.wordTitle}>{item.name}</Text>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Thumbnail square source={require('../../img/asset/home/lokasi.png')} style={styles.imageBerita} />
                        <Body style={styles.wordBerita}>
                          <Text style={styles.wordAddress}>{sliceAddress(item.location)}</Text>
                          <Text style={styles.wordRange}>{opening}</Text>
                        </Body>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
          </List>
        );
      }
    }
  }

}

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("latitude", (error, latitude) => {
      //console.log(latitude)
      AsyncStorage.getItem("longitude", (error, longitude) => {
        //console.log(longitude)
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
          },
          body: JSON.stringify({ gps: latitude + ',' + longitude, radius: "500", types: "restaurant" })
        })
          .then(response => response.json())
          .then((data) => {
            resolve(data);
          })
          .catch((e) => {
            reject(e);
          })
      })
    })
  })
}

function sliceAddress(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

const styles = StyleSheet.create({
  imageBerita: {
    borderRadius: 3,
    marginLeft: "3%",
    marginRight: "3%",
    marginBottom: "3%",
    width: 30,
    height: 30,
  },
  wordBerita: {
    alignItems: 'flex-start',
  },
  wordRange: {
    fontFamily: 'Ubuntu_Regular',
    fontSize: 10,
    color: "#363636",
  },
  wordTitle: {
    paddingLeft: "3%",
    paddingRight: "3%",
    fontFamily: 'Gotham_Medium',
    fontSize: 16,
  },
  wordAddress: {
    fontFamily: 'Ubuntu_Light',
    fontSize: 12,
  },

});